import math

import nltk.data
import re

tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
t = open("Saipem_2016_AR.1.txt", "r")
s = open("Saipem_2017_AR.1.txt", "r")
data=t.read()
dete=s.read()
aa=('\n\n'.join(tokenizer.tokenize(data)))
bb=('\n\n'.join(tokenizer.tokenize(dete)))


r = open("seeResult.txt", "w")

t1 = re.split("\n\n", aa)
t2 = re.split("\n\n", bb)


def f7(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


textArr0 = f7(t1)
textArr1 = f7(t2)


# textArr0=list(set(textArr0))
# textArr1=list(set(textArr0))


def get_words_from_line_list(L):
    word_list = []
    for line in L:
        words_in_line = get_words_from_string(line)
        word_list.append(words_in_line)
    return word_list


def get_words_from_string(line):
    word_list = []  # accumulates words in line
    character_list = []  # accumulates characters in word
    for c in line:
        if c.isalnum():
            character_list.append(c)
        elif len(character_list)>0:
            word = ("".join(character_list))
            word = word.lower()
            word_list.append(word)
            character_list = []
    if len(character_list) > 0:
        word = ("".join(character_list))
        word = word.lower()
        word_list.append(word)
    return word_list


def count_frequency(word_list):
    L = []
    for new_word in word_list:
        for entry in L:
            if new_word == entry[0]:
                entry[1] = entry[1] + 1
                break
        else:
            L.append([new_word, 1])
    return L


def word_frequencies_for_file(filename):
    line_list = filename
    word_list = get_words_from_line_list(line_list)
    freq_mapping = count_frequency(word_list)
    return freq_mapping


def inner_product(L1, L2):
    sum = 0.0
    for word1, count1 in L1:
        for word2, count2 in L2:
            if word1 == word2:
                sum += count1 * count2
    return sum


def vector_angle(L1, L2):
    numerator = inner_product(L1, L2)
    denominator = math.sqrt(inner_product(L1, L1) * inner_product(L2, L2))
    return math.acos(numerator / denominator)


def main(filename_1, filename_2):
    sorted_word_list_1 = word_frequencies_for_file(filename_1)
    sorted_word_list_2 = word_frequencies_for_file(filename_2)
    distance = vector_angle(sorted_word_list_1, sorted_word_list_2)
    return distance


# textArr0=['Hey buddy go to the cinema ' , 'Just respect your friends','Say me hello']
# textArr1=['Hey buddy name of the cinema', 'Tell someone Hello ' , 'Tell me hello']
def lcs(S,T):
    m = len(S)
    n = len(T)
    counter = [[0]*(n+1) for x in range(m+1)]
    longest = 0
    lcs_set = set()
    for i in range(m):
        for j in range(n):
            if S[i] == T[j]:
                c = counter[i][j] + 1
                counter[i+1][j+1] = c
                if c > longest:
                    lcs_set = set()
                    longest = c
                    lcs_set.add(S[i-c+1:i+1])
                elif c == longest:
                    lcs_set.add(S[i-c+1:i+1])
    return lcs_set

def lst_from_str(str):
    lst = []
    for word in str.split():
        lst.append(word)
    return lst

# count = len(textArr0) * len(textArr1)
# def print_out(textArr0,textArr1):
temp = 0
iter = 0
results = []
for str0 in textArr0:
    ls1= lst_from_str(str0)
    for str1 in textArr1:
        ls2 = lst_from_str(str1)
        diff = main(ls2, ls1)
        if diff >= 1.5:
            continue

        temp = diff
        results.append({
            'iter': iter,
            'text1': str0,
            'text2': str1,
            'diff': temp})


 
for result in results:
    if result['diff']!=temp:
        continue
    # print("---------->1 " ,result['text1'], "--------->2", result['text2'])
    l1=[i for i, j in zip(result['text1'].split(),result['text2'].split()) if i!=j]
    l2=[i for i, j in zip(result['text2'].split(),result['text1'].split()) if i!=j]

    def make_upp(lstr1, l_list):
        l=[]
        for i in lstr1.split():
            if i in l_list:
                l.append(i.upper())
            else:
                l.append(i)
        a=(" ".join(l))
        return a
    print(make_upp(result['text1'],l1))
    print(make_upp(result['text2'],l2))


            # ansLCS = lcs(result['text1'],result['text2'])
        # for s in ansLCS:
        #     print(s)
        #     ans = s
        #     break
        # empty = ' '
        # pos1 = text1.find(ans)
        # pos2 = text2.find(ans)
        # dif = pos1-pos2
        # if dif > 0:
        #     text2 = empty * dif + text2
        # elif dif < 0:
        #     text1 = empty * abs(dif) + text1

        # tdif = len(text1) - len(text2)
        # if tdif > 0 :
        #     text2 = text2 + empty * tdif
        # elif tdif < 0 :
        #     text1 = text1 + empty * abs(tdif)
        # for i in range(len(text1)):
        #     if text1[i].lower()==text2[i].lower():
        #             continue








